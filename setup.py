import os
import re
import setuptools


with open('README.md', 'r') as fh:
    long_description = fh.read()


def get_version(package):
    """
    Return package version as listed in `__version__` in `init.py`.
    """
    init_py = open(os.path.join(package, '__init__.py')).read()

    return re.search('__version__ = [\'"]([^\'"]+)[\'"]', init_py).group(1)


version = get_version('kl_cadastre_sdk')


setuptools.setup(
    name='kl-cadastre-sdk',
    version=version,
    author='Maxim Shaytanov',
    author_email='maximshaitanov@gmail.com',
    license='MIT License',
    description='SDK to work with e.land.gov.ua service.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    # url='',
    packages=setuptools.find_packages(exclude=('tests', 'tests.*')),
    install_requires=(
        'Django',
        'pyproj',
    ),
    python_requires='>=3.6',
    classifiers=(
        'Development Status :: 2 - Pre-Alpha',

        'Programming Language :: Python :: 3',
        'Framework :: Django',

        'Intended Audience :: Developers',
        'Environment :: Web Environment',
        'Topic :: Utilities',

        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ),
)

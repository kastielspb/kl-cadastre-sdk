from django.conf import settings
from django.utils.module_loading import import_string

from .serializers import (
    ELandGovInfoByCoordsSerializer,
    ELandGovParcelInfoSerializer,
)

__all__ = (
    'CADASTRE_CONFIGS',
    'get_conf_value',
    'ELANDGOV_PARCEL_INFO_SERIALIZER',
    'ELANDGOV_INFO_BY_COORDS_SERIALIZER',
)


CADASTRE_CONFIGS = getattr(settings, 'CADASTRE_CONFIGS', {})

def get_conf_value(key: str, default=None):
    path = CADASTRE_CONFIGS.get(key)
    return import_string(path) if path else default


ELANDGOV_INFO_BY_COORDS_SERIALIZER = get_conf_value(
    'elandgov_info_by_coords_serializer',
    ELandGovInfoByCoordsSerializer
)

ELANDGOV_PARCEL_INFO_SERIALIZER = get_conf_value(
    'elandgov_parcel_info_serializer',
    ELandGovParcelInfoSerializer
)

from rest_framework import serializers

from kl_cadastre_sdk.services import e_land_gov_ua

__all__ = (
    'BaseProxySerializer',
    'ELandGovInfoByCoordsSerializer',
    'ELandGovParcelInfoSerializer',
)


class BaseProxySerializer(serializers.Serializer):

    def get_results(self, data):
        return []

    def validate(self, data):
        try:
            self.results = self.get_results(data)
        except Exception as exc:
            raise serializers.ValidationError(getattr(exc, '_message', str(exc)))
        return data

    @property
    def data(self):
        data = super().data
        data['results'] = getattr(self, 'results', None) or []
        return data


class ELandGovInfoByCoordsSerializer(BaseProxySerializer):
    lat = serializers.FloatField(write_only=True)
    lng = serializers.FloatField(write_only=True)

    class Meta:
        fields = ('lat', 'lng')

    def get_results(self, data):
        return e_land_gov_ua.get_info_by_coords(
            lat=data['lat'],
            lng=data['lng'],
        )


class ELandGovParcelInfoSerializer(BaseProxySerializer):
    cadastre = serializers.CharField(write_only=True)

    class Meta:
        fields = ('cadastre', )

    def get_results(self, data):
        return e_land_gov_ua.get_parcel_info(cad_num=data['cadastre'])

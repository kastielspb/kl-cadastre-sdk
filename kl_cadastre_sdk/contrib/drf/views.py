from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .const import (
    ELANDGOV_INFO_BY_COORDS_SERIALIZER,
    ELANDGOV_PARCEL_INFO_SERIALIZER,
)

__all__ = (
    'BaseProxyAPIView',
    'ELandGovInfoByCoordsApiView',
    'ELandGovParcelInfoApiView',
)


class BaseProxyAPIView(GenericAPIView):
    permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)


class ELandGovInfoByCoordsApiView(BaseProxyAPIView):
    serializer_class = ELANDGOV_INFO_BY_COORDS_SERIALIZER


class ELandGovParcelInfoApiView(BaseProxyAPIView):
    serializer_class = ELANDGOV_PARCEL_INFO_SERIALIZER

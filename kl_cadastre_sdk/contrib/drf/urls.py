from django.conf.urls import include, url

from .views import *


urlpatterns = [
    url(r'^cadastre/', include(((
        url(r'elandgov/', include(((
            url(
                r'info-by-coords/$',
                ELandGovInfoByCoordsApiView.as_view(),
                name='info_by_coords'
            ),
            url(
                r'parcel-info/$',
                ELandGovParcelInfoApiView.as_view(),
                name='parcel_info'
            ),
        ), 'elandgov'))),
    ), 'cadastre'))),
]

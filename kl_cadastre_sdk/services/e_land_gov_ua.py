import requests
import re
from typing import Dict
from pyproj import Proj, transform

from django.core.cache import cache
from django.conf import settings


base_url = 'https://e.land.gov.ua'
access_cache_key = 'elandgov_access_token'
access_cache_delta = 200



def get_credentials():
    # TODO: add client class to rm dependency of django modules
    return {
        'client_id': getattr(settings, 'E_LAND_GOV_ID', None),
        'client_secret': getattr(settings, 'E_LAND_GOV_SECRET', None),
    }

def get_access_token():
    token = cache.get(access_cache_key)
    if token:
        return token

    authorize()
    token = cache.get(access_cache_key)
    if token:
        return token

    return None

def get_header(url=None, origin=None):
    return {
        "authorization": f"Bearer {get_access_token()}",
        "accept": "application/json",
        'Referer': url if url else base_url,
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': origin if origin else base_url,
        'Host': "e.land.gov.ua",
    }

def get_post_data(url: str, data: Dict):
    return (
        requests
        .post(
            data=data,
            headers=get_header(url=f'{base_url}{url}'),
            url=f'{base_url}{url}',
        )
        .json()
    )

def set_auth_cache(data: Dict):
    cache.set(
        access_cache_key,
        data.get('access_token'),
        int(data.get('expires_in', 3600)) - access_cache_delta,
    )

def authorize():
    credentials = get_credentials()
    if not credentials:
        return

    headers = {
        'Referer': f'{base_url}/oauth/v2/token',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': base_url,
        'Host': "e.land.gov.ua",
    }
    data = (
        requests
        .post(
            f'{base_url}/oauth/v2/token',
            data={'grant_type': 'client_credentials', **credentials},
            headers=headers,
        )
        .json()
    )

    if data and 'access_token' in data.keys():
        set_auth_cache(data)
    return

def convert_epsg3857_to_epsg4326(lng, lat):
    return transform(Proj(init='epsg:3857'), Proj(init='epsg:4326'), lng, lat)

def convert_epsg4326_to_epsg3857(lng, lat):
    return transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'), lng, lat)

def nornalize_responce(data):
    for item in data:
        if not (item and 'ppoint' in item):
            continue

        coords = re.findall("\d+\.\d+", item['ppoint'])
        item['lng'], item['lat'] = (
            convert_epsg3857_to_epsg4326(lng=coords[0], lat=coords[1])
            if len(coords) == 2
            else (0, 0)
        )
    return data

def get_parcel_info(cad_num: str):
    token = get_access_token()
    if not token:
        print('It is not available take access token for ELandGovUa')
        return

    credentials = get_credentials()
    data = get_post_data(
        url='/api/parcel_info',
        data={
            "cad_num": cad_num,
            "request_owner": credentials.get('client_id'),
            "request_owner_id": ""
        }
    )
    return nornalize_responce(data)

def get_info_by_coords(lat: float, lng: float):
    token = get_access_token()
    if not token:
        print('It is not available take access token for ELandGovUa')
        return

    data = get_post_data(
        url='/api/info_by_coords',
        data={"lat": lng, "lng": lat}  # wrong on elandgov api side
    )
    return nornalize_responce(data)
